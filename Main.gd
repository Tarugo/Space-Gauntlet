extends Node2D

var map1 = preload("res://Map1.tscn")

@onready var mapa = map1.instantiate()
var final = 0


# Called when the node enters the scene tree for the first time.
func _ready():
	Global.gameOver = 0
	Global.changeMap.connect(_on_changeMap)
	var marker = mapa.get_node("PlayerPosition")
	$Player.position = marker.position
	Global.score = 0
	$HUD.update_score(Global.score)
	Global.life = 1000
	$HUD.update_life(Global.life)
	Global.key = 0
	get_parent().add_child(mapa)

	$HUD/Message.text = "¡Escapa del laberinto!"
	$HUD/Message.show()
	await get_tree().create_timer(1.0).timeout
	$HUD/Message.hide()


func _process(_delta):
	$HUD.update_score(Global.score)
	$HUD.update_life(Global.life)
	$HUD.update_key(Global.key)
	

func game_over():
	Global.gameOver = 1
	get_tree().call_group("enemy", "queue_free")
	$Player.hide()
	get_parent().remove_child(mapa)
	$HUD.show_game_over()
	

	
func new_game():
	var marker = mapa.get_node("PlayerPosition")
	
	Global.life = 1000
	Global.score = 0
	Global.key = 0
	$HUD.update_score(Global.score)
	$HUD.update_life(Global.life)
	mapa = map1.instantiate()
	get_parent().add_child(mapa)
	$Player.show()
	$HUD.show_message("Escapa del laberinto!")
	$Player.position = marker.position
	Global.gameOver = 0

	

func _on_player_pupa():
	if Global.player_attacked == 1:
		await get_tree().create_timer(0.5).timeout
		Global.life = Global.life - 100
		$HUD.update_life(Global.life)
		if Global.life < 1:
			game_over()
	Global.player_attacked = 0


func _on_changeMap(next):
	print(Global.currentMap)
	print ("cambiamos de level")
	print (next)
	var targetLevel

	if next == "map2":
		print ("next map 2")
		next = preload("res://Map2.tscn")
		$HUD.show_message("Level 2")

	elif next == "map3":
		print ("next map 3")
		next = preload("res://Map3.tscn")
		$HUD.show_message("Level 3")
		
	elif next == "map4":
		print ("next map 4")
		next = preload("res://Map4.tscn")
		$HUD.show_message("Level 4")
		
	elif next == "map5":
		print ("next map 5")
		next = preload("res://Map5.tscn")
		$HUD.show_message("Level 5")
		
	elif next == "ending":
		final = 1
		print ("ending scene")
		next = preload("res://Ending.tscn")
		targetLevel = next.instantiate()
		get_parent().call_deferred("remove_child", mapa)
		$Player.hide()
		#mapa.queue_free()
		get_parent().call_deferred("add_child", targetLevel)
		mapa = targetLevel
		next = null
		$Player.queue_free()
		#$HUD.show_message("You WON")
		#get_tree().quit()
	
	if final == 0:
		mapa.queue_free()
		targetLevel = next.instantiate()
		$Player.position = targetLevel.get_node("PlayerPosition").global_position
		get_parent().call_deferred("remove_child", mapa)
		$Player.hide()
		await get_tree().create_timer(2).timeout
		$Player.show()
		get_parent().call_deferred("add_child", targetLevel)
		mapa = targetLevel
		next = null
	#$Player.position = targetLevel.get_node("PlayerPosition").global_position	
	
	#get_tree().change_scene_to_file(targetLevel)

