extends Node2D
#MAP 1


var next_scene
var mapa


func _ready():
	Global.currentMap = "uno"


func _process(_delta):
	pass


func _on_exit_body_entered(body):
	if (Global.currentMap == "uno") and (body.is_in_group("player")):
		#$ExitSound.play()
		Global.get_exit_sound.emit()
		next_scene = "map2"
		Global.next_scene = next_scene
		Global.changeMap.emit(Global.next_scene)
	




func _on_exit_map_4_body_entered(body):
	if (Global.currentMap == "uno") and (body.is_in_group("player")):
		#$ExitSound.play()
		Global.get_exit_sound.emit()
		next_scene = "map4"
		Global.next_scene = next_scene
		Global.changeMap.emit(Global.next_scene)
	


func _on_exit_map_8_body_entered(body):
	if (Global.currentMap == "uno") and (body.is_in_group("player")):
		#$ExitSound.play()
		Global.get_exit_sound.emit()
		next_scene = "map3"
		Global.next_scene = next_scene
		Global.changeMap.emit(Global.next_scene)
