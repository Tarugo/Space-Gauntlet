
# Space Gauntlet

Space Gauntlet nació en 2022 como idea para crear un GAUNTLET (1985) actualizado y clonado con los nuevos motores para videojuegos. Pretende ser un pequeño homenaje a aquel gran juego que nos hacía perdernos entre infinitos laberintos conociendo innumerables tipos de monstruos.

Space Gauntlet queda muy lejos de parecérsele un poco. Sin embargo, este proyecto presenta las bases para quien quiera cogerlo con las mecánicas y el core ya realizado. Puedes descargarlo, cambiar los sprites, añadir nuevos enemigos, nuevos mapas y laberintos... y acomododarlo a tu gusto. 

Este código ya incluye las mecánicas y la programación para mater enemigos, las casas de spawn, puntuación, vida, llaves, puertas, paso de niveles... 

Está realizado bajo Godot 4 y se distribuye bajo liencia Creative Commons Atribución-NoComercial-CompartirIgual 4.0 Internacional.

No dudes en contactarme para cualquier sugerencia o cambio.

Quizás en el futuro vuelva a tener el tiempo suficiente para dedicarle a este proyecto y poder un juego más completo. Ahora mismo, consta de sólo 5 niveles. ¡No te olvides de avisarme por Twitter (@Trevizer) o Mastodon (@Trevizer@tkz.one) si logras completarlo!



Descarga el juego en la carpeta Lanzamientos:

[Primera versión](https://codeberg.org/Trevizer/Space-Gauntlet/releases)
