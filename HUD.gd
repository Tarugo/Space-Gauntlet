extends CanvasLayer


signal start_game
var show_instructions = 0


# Called when the node enters the scene tree for the first time.
func _ready():
	$StartButton.hide()
	$Instructions.hide()
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if Input.is_action_just_pressed("ui_escape"):
		if show_instructions == 0:
			show_instructions = 1
			$Instructions.show()
			get_tree(). paused = true
		else:
			show_instructions = 0
			$Instructions.hide()
			get_tree(). paused = false
			
	pass



func show_message(text):
	$Message.text = text
	$Message.show()
	$MessageTimer.start()
	
	
func show_game_over():
	show_message("Game Over")
	# Wait until the MessageTimer has counted down.
	await $MessageTimer.timeout
	$Message.text = "¡Vuelve a intentarlo!"
	$Message.show()
	# Make a one-shot timer and wait for it to finish.
	await get_tree().create_timer(1.0).timeout
	$StartButton.show()
	
	
func update_score(score):
	$ScoreLabel.text = str(Global.score)
	
func update_life(life):
	$LifeLabel.text = str(Global.life)

	
func update_key(key):
	$KeyLabel.text = str(Global.key)
	

func _on_start_button_pressed():
	$StartButton.hide()
	start_game.emit()
	#pass # Replace with function body.
	



func _on_message_timer_timeout():
	$Message.hide()
	#pass # Replace with function body.
