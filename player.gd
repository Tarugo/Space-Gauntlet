extends CharacterBody2D

@export var speed = 60



const bulletPath = preload("res://bullet.tscn")
signal pupa

var cogiendo_llave = 0
var screen_size 
var turbo = 0
var bajando_puntuacion = 0
#var turbo = 0

func _ready():
	add_to_group("player")
	$healthUP.hide()
	Global.sound_enemyhouse.connect(_on_enemysound)
	Global.get_exit_sound.connect(_on_exitsound)

func _physics_process(_delta):
	Global.player_position = self.global_position
	var direction = Vector2(
		Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left"),
		Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
		)
	
	direction = direction.normalized()	
	
#FUNCIÓN PARA TURBO

	if Input.is_action_pressed("ui_turbo"):
		if Global.score > 200 and turbo == 0:
			turbo = 1
			speed = speed * 2


						
	if Input.is_action_just_released("ui_turbo"):
		if turbo == 1 and bajando_puntuacion == 1:
			speed = speed / 2
			turbo = 0
			
	if turbo == 1:
		bajar_puntuacion()

			


	###### FIN FUNCIÓN PARA TURBO #####
		
		
	velocity = direction * speed
	if direction:
		set_rotation(int(direction.angle() / TAU * 8) / 8.0 * TAU)
	if Global.gameOver == 0:
		move_and_slide()
	for i in get_slide_collision_count():
		var collision = get_slide_collision(i)
		choque(collision.get_collider())

		
	if Input.is_action_just_pressed("ui_fire") and Global.gameOver == 0:
		$Marker2D.position = position #posición inicial de la bala
		shoot()


#CUANDO SE USA EL TURBO, SE GASTA PUNTUACIÓN
func bajar_puntuacion():
	if bajando_puntuacion == 0:
		bajando_puntuacion = 1
		Global.score = Global.score - 200
			
		if Global.score < 200:
			turbo = 0	
			speed = speed / 2			
		await get_tree().create_timer(0.5).timeout
		bajando_puntuacion = 0


##  FUNCIÓN DE DISPARO  ###
func shoot():
	var direction = Vector2(1,0).rotated($AnimatedSprite2D.global_rotation)
	var bullet = bulletPath.instantiate()
	get_parent().add_child(bullet)
	bullet.position = $Marker2D.position
	bullet.direction = direction
	bullet.rotation = direction.angle()
	$Fire.play()
	
### FUNCIÓN DE CONTACTO CON UN EMEMIGO ###
func choque(collider):

	if collider.is_in_group("enemy"):
		if Global.player_attacked == 0:
			Global.player_attacked = 1
			##si queremos que el enemigo muera al hacer daño##
			collider.queue_free()
			Global.enemydead.emit(collider)
			#############################
			$Enemy_attack.play()
			pupa.emit()
			
	elif collider.is_in_group("door"):
		if Global.key > 0:
			$Use_key.play()
			collider.queue_free()
			Global.key = Global.key - 1
			
	elif collider.is_in_group("key"):
		if cogiendo_llave == 0:
			print(cogiendo_llave)
			cogiendo_llave = 1
			collider.queue_free()
			$Pickup_key.play()
			print ("+1 key")
			Global.key = Global.key + 1 
			print ("llave eliminada")
			print(cogiendo_llave)
			await get_tree().create_timer(0.5).timeout
			cogiendo_llave = 0
		
	elif collider.is_in_group("life"):
			$Get_life.play()
			Global.life = Global.life + 200
			print("+200 salud")
			collider.queue_free()
			



func _on_enemysound(sound):
	if sound == "first":
		$First_enemyHouse.play()
	else:
		$Second_enemyHouse.play()
	

func _on_exitsound():
	$Get_Exit.play()
