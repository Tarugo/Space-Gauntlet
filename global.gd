extends Node
#GLOBAL

#variable que recoge la puntuación
var score: int

#variable que recoge la salud
var life: int

#variable que recoge la cantidad de llaves que tiene el player
var key: int

#variable que recoge la posición del player para pasársela como target a los enemigos
var player_position

#variable que detecta si el player está siendo atacado
var player_attacked = 0

#señal para cuando muere un enemigo y queremos saber a qué casa pertenecía
signal enemydead(body)

#señal para el paso de nivel
signal changeMap(map)

signal sound_enemyhouse(sound) #para el sonido del daño al enemyhouse

signal get_exit_sound(sound) #para el sonido de cuando coge una salida

#variable para saber si la partida ha terminado
var gameOver: int 

var currentMap = null

var next_scene = null

# Called when the node enters the scene tree for the first time.
func _ready():
	
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

