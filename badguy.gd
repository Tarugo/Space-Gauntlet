extends CharacterBody2D


@export var speed = 50
#@onready var player = get_node("/root/Main/Player")
var player
var target = null
#const player = preload("res://player.tscn")

func _ready():
	#player = get_node("/Main/Player")
	add_to_group("enemy")
	#target = get_node("/root/Main/Player")
	target = Global.player_position

func _physics_process(_delta):
	var direction = (Global.player_position - global_position).normalized()
	velocity = direction * speed
	look_at(Global.player_position)
	move_and_slide()
	

